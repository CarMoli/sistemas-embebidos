/**	@brief muestreo.c, este
*	programa genera una señal
*	senoidal y la guarda en 
*	un archivo
*/
#include<stdio.h>
#include "archivos.h"
#include "procesamiento.h"
#include "defs.h"

int main(){
	float seno[MUESTRAS];
	generaSeno(seno);
	guardaDatos(seno);
	return 0;
}