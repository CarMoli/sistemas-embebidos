// hashtag no homo
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

int num1 = 20, num2 = 10;

int * reservar_memoria(void);
void llenar_arreglo(int *datos);
void imprimir(int *datos);

int buscar_mayor(int *datos);
int buscar_menor(int *datos);

int main()
{
	int *res_mayor, *res_menor;
	pthread_t tid_mayor, tid_menor;


	pthread_create( &tid_mayor, NULL, buscar_mayor, NULL);
	pthread_create( &tid_menor, NULL, buscar_menor, NULL);

	pthread_join(tid_suma, (void **) &res_suma);
	pthread_join(tid_resta, (void **) &res_resta);
	pthread_join(tid_multi, (void **) &res_multi);
	pthread_join(tid_divi, (void **) &res_divi);

	printf("La suma es %d\n",*res_suma);
	printf("La resta es %d\n",*res_resta);
	printf("La multi es %d\n",*res_multi);
	printf("La divi , es %d\n",*res_divi);



	return 0;
}

void *suma (void *arg)									// Nota:
{														// En las variables locales, para evitar que se "destruya"
		static int res;									// la variable, se debe declarar static o se deben de 
														// hacer apuntadores.  
		res = num1 + num2;
		pthread_exit((void *) &res);
} 

void *resta (void *arg)
{
		static int res;

		res = num1 - num2;
		pthread_exit((void *) &res);
} 

void *multi (void *arg)
{
		int *res = (int *)malloc(sizeof(int));

		*res = num1 * num2;
		pthread_exit((void *) res);
} 

void *divi (void *arg)
{
		int *res = (int *)malloc(sizeof(int));

		*res = num1 / num2;
		pthread_exit((void *)res);
} 

int buscar_mayor(int *datos)
{
	register int i;
	int mayor;

	mayor = datos[0];
	for ( i = 0; i < N; i++)
	{
		if (datos[i] > mayor)
		{
			mayor = datos[i];
		}//if
	}
	return mayor;
}//buscar_mayor

int buscar_menor(int *datos)
{
	register int i;
	int menor;

	menor = datos[0];
	for (i = 0; i < N; i++)
	{
		if (datos[i] < menor)
		{
			menor = datos[i];
		}//if
	}//for
	return menor;
}

void imprimir(int *datos)
{
	register int i;
	for (i = 0; i < N; i++)
	{
		if (!(i%16))
		{
			printf("\n");
		}//if
		printf("%3d ",datos[i]);
	}//for
	printf("\n");
}//imprimir

void llenar_arreglo(int *datos)
{
	register int i;
	for (i = 0; i < N; i++)
	{
		datos[i] = rand() % 256;
	}//for
}//llenar_arreglo

int * reservar_memoria(void)
{

	int *mem;

	mem = (int*)malloc( sizeof(int)*N);
	if (mem == NULL)
	{
		perror("Error de asignacion de memoria\n");
		exit(EXIT_FAILURE);
	}//if
	return mem;
}//reservar_memoria


