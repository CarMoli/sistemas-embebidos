#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#include "defs.h"

int *reservar_memoria(void){
	int *mem;
	mem = (int *)malloc(sizeof(int)*N);
	if(mem == NULL){
		perror("Error de asignacion de memoria");
		exit(EXIT_FAILURE);
	}
	return mem;
}
void llenar_arreglo(int *datos){
	register int i;

	for(i=0; i<N; i++){
		datos[i] = rand()%NUM_DAT;
	}
}
void imprimir(int *datos){
	register int i;
	for(i=0; i<N; i++){
		if(!(i%16))
			printf("\n");
		printf("%3d ", datos[i]);
	}
	printf("\n");
}

void imprimir_histo(int *cont){
	register int i, j;
	
	printf("\n%s\n%s\n%s\n", "******", "MODA", "******");
	printf("%s%12s%12s\n", "Valor", "Frecuencia", "Histograma");
	for(i=0; i<NUM_DAT; i++){
		if(cont[i]!=0){
			printf("%4d%8d\t", i, cont[i]);
		}
		for(j=1; j<=cont[i]; j++){
			printf("*");
		}
		if(cont[i]!=0){
			printf("\n");
		}
	}
}