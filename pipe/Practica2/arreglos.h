#ifndef ARREGLOS_H
#define ARREGLOS_H

int *reservar_memoria(void);
void llenar_arreglo(int *datos);
void imprimir(int *datos);
void imprimir_histo(int *cont);	

#endif