#ifndef PROCESOS_H
#define PROCESOS_H

void proceso_hijo(int np, int pipefd[], int datos[], int cont[]);
void proceso_padre(int pipefd[], int *datos, int cont[]);

#endif