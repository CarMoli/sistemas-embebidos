#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#define N 32

int * reservar_memoria(void);
void llenar_arreglo(int *datos);
void imprimir(int *datos);

void * buscar_mayor(void *arg);
void * buscar_menor(void *arg);

int main()
{
	int *res_mayor, *res_menor, *datos;
	pthread_t tid_mayor, tid_menor;

	datos = reservar_memoria();
	llenar_arreglo(datos);
	imprimir(datos);

	pthread_create( &tid_mayor, NULL, buscar_mayor, (void *)datos);
	pthread_create( &tid_menor, NULL, buscar_menor, (void *)datos);

	pthread_join(tid_mayor, (void **) &res_mayor);
	pthread_join(tid_menor, (void **) &res_menor);

	printf("El mayor es %d\n",*res_mayor);
	printf("EL menor es %d\n",*res_menor);

	free(datos);
	return 0;
}

void * buscar_mayor(void *arg){
	register int i;
	static int mayor;
	int *datos=(int *)arg;

	mayor = datos[0];
	for ( i = 0; i < N; i++){
		if (datos[i] > mayor){
			mayor = datos[i];
		}//if
	}
	pthread_exit( (void *)&mayor );
}//buscar_mayor

void * buscar_menor(void *arg){
	register int i;
	static int menor;
	int *datos=(int *)arg;

	menor = datos[0];
	for (i = 0; i < N; i++){
		if (datos[i] < menor){
			menor = datos[i];
		}//if
	}//for
	pthread_exit( (void *)&menor );
}

void imprimir(int *datos){
	register int i;
	for (i = 0; i < N; i++){
		if (!(i%16)){
			printf("\n");
		}//if
		printf("%3d ",datos[i]);
	}//for
	printf("\n");
}//imprimir

void llenar_arreglo(int *datos){
	register int i;
	for (i = 0; i < N; i++){
		datos[i] = rand() % 256;
	}//for
}//llenar_arreglo

int * reservar_memoria(void){
	int *mem;

	mem = (int*)malloc( sizeof(int)*N);
	if (mem == NULL){
		perror("Error de asignacion de memoria\n");
		exit(EXIT_FAILURE);
	}//if
	return mem;
}//reservar_memoria