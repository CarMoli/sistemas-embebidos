#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#define NUM_HILOS 4

void* funHilo(void *arg);
int contador;
pthread_mutex_t bloqueo;

int main(){
	register int nh;
	int nhs[NUM_HILOS], *res_nh;
	pthread_t tids[NUM_HILOS];
	
	contador=0;
	pthread_mutex_init( &bloqueo, NULL );
	
	for( nh=0; nh<NUM_HILOS; nh++){
		nhs[nh] = nh;
		pthread_create(&tids[nh], NULL, funHilo, (void *)&nhs[nh]);
	}
	for( nh=0; nh<NUM_HILOS; nh++){
		pthread_join(tids[nh], (void **)&res_nh);
		printf("Hilo %d terminado.\n", *res_nh);
	}
	pthread_mutex_destroy( &bloqueo );

	return 0;
}

void* funHilo(void *arg){
	int nh = *(int *)arg;
	register int i = 0;

	pthread_mutex_lock( &bloqueo );
	contador++;
	printf("Hilo %d ejecutado con contador %d\n", nh, contador);

	while( (--i) );

	printf("Hilo terminado %d ejecutado con contador %d\n", nh, contador);
	pthread_mutex_unlock( &bloqueo );

	pthread_exit(arg);
}

//reentrancia
//condicion de carrera