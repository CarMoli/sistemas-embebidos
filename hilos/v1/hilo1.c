#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int num1=20, num2=10;

void* suma(void *arg);
void* resta(void *arg);
void* multi(void *arg);
void* divi(void *arg);

int main(){
	int *res_suma, *res_resta, *res_multi, *res_divi;
	pthread_t tid_suma, tid_resta, tid_multi, tid_divi;
	
	pthread_create(&tid_suma, NULL, suma, NULL);
	pthread_create(&tid_resta, NULL, resta, NULL);
	pthread_create(&tid_multi, NULL, multi, NULL);
	pthread_create(&tid_divi, NULL, divi, NULL);

	pthread_join(tid_suma, (void **)&res_suma);
	pthread_join(tid_resta, (void **)&res_resta);
	pthread_join(tid_multi, (void **)&res_multi);
	pthread_join(tid_divi, (void **)&res_divi);

	printf("La suma es %d\n", *res_suma);
	printf("La resta es %d\n", *res_resta);
	printf("La multiplicacion es %d\n", *res_multi);
	printf("La division es %d\n", *res_divi);

	return 0;
}

void* suma(void *arg){
	static int res;
	res=num1+num2;
	pthread_exit((void *)&res);
}
void* resta(void *arg){
	static int res;
	res=num1-num2;
	pthread_exit((void *)&res);
}
void* multi(void *arg){
	int *res = (int *)malloc(sizeof(int));
	*res=num1*num2;
	pthread_exit((void *)res);
}
void* divi(void *arg){
	int *res = (int *)malloc(sizeof(int));
	*res=num1/num2;
	pthread_exit((void *)res);
}