#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#define NUM_PROC 2
#define N 32

int buscar_menor(int *datos);
int buscar_mayor(int *datos);
void imprimir(int *datos);
void llenar_arreglo(int *datos);
int *reservar_memoria(void);

void proceso_hijo(int, int *);
void proceso_padre();

int main(){
	pid_t pid;
	register int np;
	int *datos;

	printf("Probando procesos...\n");

	srand(getpid());
	datos = reservar_memoria();
	llenar_arreglo(datos);
	imprimir(datos);

	for(np=0; np<NUM_PROC; np++){
		pid=fork();
		if(pid==-1){
			perror("Error al crear el proceso");
			exit(EXIT_FAILURE);
		}
		if(!pid){
			proceso_hijo(np, datos);
		}
	}
	proceso_padre();
	return 0;
}
void proceso_hijo(int np, int *datos){
	int mayor, menor;
	printf("Proceso hijo: %d, con pid %d\n", np, getpid());
	if(np==0){
		mayor = buscar_mayor(datos);
		exit(mayor);
	}
	else{
		menor = buscar_menor(datos);
		exit(menor);
	}
}
int buscar_menor(int *datos){
	register int i;
	int menor;
	menor = datos[0];
	for(i=1; i<N; i++){
		if(datos[i]<menor)
			menor = datos[i];
	}
	return menor;
}
int buscar_mayor(int *datos){
	register int i;
	int mayor;
	mayor = datos[0];
	for(i=1; i<N; i++){
		if(datos[i]>mayor)
			mayor = datos[i];
	}
	return mayor;
}
void imprimir(int *datos){
	register int i;
	for(i=0; i<N; i++){
		if(!(i%16))
			printf("\n");
		printf("%3d ", datos[i]);
	}
	printf("\n");
}
void llenar_arreglo(int *datos){
	register int i;

	for(i=0; i<N; i++){
		datos[i] = rand()%256;
	}
}
int *reservar_memoria(void){
	int *mem;
	mem = (int *)malloc(sizeof(int)*N);
	if(mem == NULL){
		perror("Error de asignacion de memoria");
		exit(EXIT_FAILURE);
	}
	return mem;
}
void proceso_padre(){
	pid_t pid;
	register int np;
	int estado;
	printf("Proceso padre con pid %d\n", getpid());
	for(np=0; np<NUM_PROC; np++){
		pid=wait(&estado);
		printf("Proceso hijo con pid: %d y retorno %d terminado\n", pid, estado>>8);
	}
}

/*
*	Busca el mayor->P1
*	Busca el menor->P2
*	Promedio(entero)->P3
*	El que mas se repite->P4
*	Modulado
*	principal.c
*		main
*	procesos-c
*		proceso_hijo
*		proceso_padre
*	procesamiento.c
*		Busca_mayor
*		Busca_menor
*		promedio
*		frecuencia
*	arreglos.c
*		reservar_memoria
*		llenar_arreglo
*		imprimir
*/