#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#include "defs.h"

int buscar_mayor(int *datos){
	register int i;
	int mayor;
	mayor = datos[0];
	for(i=1; i<N; i++){
		if(datos[i]>mayor)
			mayor = datos[i];
	}
	return mayor;
}
int buscar_menor(int *datos){
	register int i;
	int menor;
	menor = datos[0];
	for(i=1; i<N; i++){
		if(datos[i]<menor)
			menor = datos[i];
	}
	return menor;
}
float promedio(int *datos){
	register int i;
	float prom = datos[0];
	for(i=1; i<N; i++){
		prom += datos[i];
	}
	prom = prom/N;
	return prom;
}
int frecuencia(int *datos){
	register int i,j;
	int temp=0, moda=0;
	int cont[NUM_DAT]={0} /**contador*/;
	//contador = cont;
	for(i=0; i<NUM_DAT; i++){
		for(j=0; j<N; j++){
			if(i==datos[j]){
				cont[i]++;
			}
		}
	}
	for(i=0; i<NUM_DAT; i++){
		if(temp<=cont[i]){
			temp = cont[i];
			moda = i;
		}
	}
	return moda;
}