#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#include "defs.h"
#include "procesamiento.h"

void proceso_hijo(int np, int *datos){
	int mayor, menor, moda;
	float prom;
	printf("Proceso hijo: %d, con pid %d.\n", np, getpid());
	if(np==0){
		mayor = buscar_mayor(datos);
		printf("MAYOR: %d\n", mayor);
		exit(mayor);
	}
	else if(np==1){
		menor = buscar_menor(datos);
		printf("MENOR: %d\n", menor);
		exit(menor);
	}
	else if(np==2){
		prom = promedio(datos);
		printf("PROMEDIO: %.2f\n", prom);
		exit(prom);
	}
	else{
		moda = frecuencia(datos);
		printf("MODA: %d\n", moda);
		exit(moda);
	}
}
void proceso_padre(){
	pid_t pid;
	register int np;
	int estado;
	printf("Proceso padre con pid %d\n", getpid());
	for(np=0; np<NUM_PROC; np++){
		pid=wait(&estado);
		printf("Proceso hijo %d con pid: %d y retorno %.d, terminado.\n\n", np, pid, estado>>8);
	}
}