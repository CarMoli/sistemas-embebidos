#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include "defs.h"
#include "arreglos.h"
#include "procesos.h"
#include "procesamiento.h"

int main(){
	pid_t pid;
	register int np;
	int *datos;

	printf("Probando procesos...\n");

	srand(getpid());
	datos = reservar_memoria();
	llenar_arreglo(datos);
	imprimir(datos);

	for(np=0; np<NUM_PROC; np++){
		pid=fork();
		if(pid==-1){
			perror("Error al crear el proceso");
			exit(EXIT_FAILURE);
		}
		if(!pid){
			proceso_hijo(np, datos);
		}
	}
	proceso_padre();	
	return 0;
}
/*
*	Busca el mayor->P1
*	Busca el menor->P2
*	Promedio(entero)->P3
*	El que mas se repite->P4
*	Modulado
*	principal.c
*		main
*	procesos-c
*		proceso_hijo
*		proceso_padre
*	procesamiento.c
*		Busca_mayor
*		Busca_menor
*		promedio
*		frecuencia
*	arreglos.c
*		reservar_memoria
*		llenar_arreglo
*		imprimir
*/