#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#define N 32

int buscar_menor(int *datos);
int buscar_mayor(int *datos);
void imprimir(int *datos);
void llenar_arreglo(int *datos);
int *reservar_memoria(void);

int main(){
	int *datos, mayor, menor;
	datos = reservar_memoria();
	llenar_arreglo(datos);
	imprimir(datos);

	mayor = buscar_mayor(datos);
	menor = buscar_menor(datos);
	
	printf("El numero mayor es: %d\n", mayor);
	printf("El numero menor es: %d\n", menor);
	free(datos);
	return 0;
}
int buscar_menor(int *datos){
	register int i;
	int menor;
	menor = datos[0];
	for(i=1; i<N; i++){
		if(datos[i]<menor)
			menor = datos[i];
	}
	return menor;
}
int buscar_mayor(int *datos){
	register int i;
	int mayor;
	mayor = datos[0];
	for(i=1; i<N; i++){
		if(datos[i]>mayor)
			mayor = datos[i];
	}
	return mayor;
}
void imprimir(int *datos){
	register int i;
	for(i=0; i<N; i++){
		if(!(i%16))
			printf("\n");
		printf("%3d ", datos[i]);
	}
	printf("\n");
}
void llenar_arreglo(int *datos){
	register int i;

	for(i=0; i<N; i++){
		datos[i] = rand()%256;
	}
}

int *reservar_memoria(void){
	int *mem;
	mem = (int *)malloc(sizeof(int)*N);
	if(mem == NULL){
		perror("Error de asignacion de memoria");
		exit(EXIT_FAILURE);
	}
	return mem;
}