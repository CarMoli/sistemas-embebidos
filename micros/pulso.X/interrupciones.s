    .include "p30F3013.inc"
    .GLOBAL __T3Interrupt
    .GLOBAL __ADCInterrupt
   
__T3Interrupt:
    
    BTG	    LATD,   #LATD8
    NOP
    
    BCLR    IFS0,   #T3IF
    RETFIE

__ADCInterrupt:
    PUSH    W0
    
    MOV	    ADCBUF0,	W0	    ;W0=ADCBUF0
    LSR	    W0,		#4,	W0  ;W0=W0>>4
    MOV	    W0,		U1TXREG	    ;U1TXREG=WO
    
    BCLR    IFS0,       #ADIF
    POP	    W0
    RETFIE
