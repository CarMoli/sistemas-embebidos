#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include "defs.h"

extern float seno[MUESTRAS], rela[MUESTRAS];

void guardaDatos(float datos[], float rela[]){
	FILE *apArch, *apRela;
	register int n;
	apArch=fopen("seno.dat","w");
	apRela=fopen("correlacion.dat","w");
	if(!apArch || !apRela){
		perror("Error al abrir el archivo");
		exit(EXIT_FAILURE);
	}
	for(n=0; n<MUESTRAS; n++){
		fprintf(apArch, "%f\n", datos[n]);
		fprintf(apRela, "%f\n", rela[n]);
	}
	fclose(apArch);
	fclose(apRela);
}