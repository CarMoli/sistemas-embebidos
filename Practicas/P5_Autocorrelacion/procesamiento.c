#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include "defs.h"

extern float seno[MUESTRAS], rela[MUESTRAS];

void generaSeno(float seno[]){
	float f=1.3, fs=512;
	register int n;
	for(n=0; n<MUESTRAS; n++){
		seno[n] = sinf(2*M_PI*n*f/fs);
	}
}