#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include "defs.h"
#include "archivos.h"
#include "hilos.h"
#include "procesamiento.h"

float seno[MUESTRAS], rela[MUESTRAS];
void puntoMax(float *rela);

pthread_mutex_t bloqueo;

int main(){
	register int nh;
	int nhs[NUM_HILOS], *res_nh;
	pthread_t tids[NUM_HILOS];
	
	generaSeno(seno);

	pthread_mutex_init(&bloqueo, NULL);

	for( nh=0; nh<NUM_HILOS; nh++){
		nhs[nh] = nh;
		pthread_create(&tids[nh], NULL, funHilo, (void *)&nhs[nh]);
	}
	for( nh=0; nh<NUM_HILOS; nh++){
		pthread_join(tids[nh], (void **)&res_nh);
		printf("Hilo %d terminado.\n", *res_nh);
	}
	pthread_mutex_destroy(&bloqueo);
	puntoMax(rela);

	guardaDatos(seno, rela);
	
	return 0;
}

void puntoMax(float *rela){
	int i,pulsosTotales=0;
	float pulsosXmin=0;

	float num1,num2=0;

	for(i=0; i<MUESTRAS; i++){
		num1=rela[i] - rela[i+1];
		if(num1>0 && num2<0){
			printf("Punto máximo en la muestra %d con valor %f\n", i, rela[i]);
			pulsosTotales++;
		}
		num2=num1;
	}
	printf("PULSOS TOTALES: %d\n", pulsosTotales);
	pulsosXmin = (((MUESTRAS/(512*(float)pulsosTotales)))*60);
	printf("PULSOS X MINUTO: %f\n", pulsosXmin);
}