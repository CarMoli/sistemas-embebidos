#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include "defs.h"

extern float seno[MUESTRAS], rela[MUESTRAS];
extern pthread_mutex_t bloqueo;

void* funHilo(void *arg){
	int nh = *(int *)arg;
	register int l = 0;
	register int n;

	printf("Hilo %d ejecutado con contador\n", nh);
	pthread_mutex_lock(&bloqueo);
	for(l=nh; l<MUESTRAS; l++){
		rela[l] = 0;
		for(n=l; n<MUESTRAS; n++){
			rela[l] += seno[n]*seno[n-l];
		}
	}
	pthread_mutex_unlock(&bloqueo);

	pthread_exit(arg);
}


/*
393,
774,
1181,
1575,
1969,
2362,
2756,
3149,
3542,
3931
*/