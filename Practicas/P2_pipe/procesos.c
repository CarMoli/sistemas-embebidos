#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#include "defs.h"
#include "procesamiento.h"
#include "arreglos.h"

void proceso_hijo(int np, int pipefd[], int datos[], int cont[]){
	int mayor, menor, prom, moda;
	printf("Proceso hijo: %d, con pid %d\n", np, getpid());
	
	close(pipefd[0]);
	if(np==0){
		mayor = buscar_mayor(datos);
		printf("MAYOR: %d\n", mayor);
		write(pipefd[1], &mayor, sizeof(int));
		close(pipefd[1]);
		exit(np);
	}
	else if(np==1){
		menor = buscar_menor(datos);
		printf("MENOR: %d\n", menor);
		write(pipefd[1], &menor, sizeof(int));
		close(pipefd[1]);
		exit(np);
	}
	else if(np==2){
		prom = promedio(datos);
		printf("PROMEDIO: %d\n", prom);
		write(pipefd[1], &prom, sizeof(int));
		close(pipefd[1]);
		exit(np);
	}
	else if(np==3){
		moda = frecuencia(datos);
		printf("MODA: %d\n", moda);
		write(pipefd[1], &moda, sizeof(int));
		close(pipefd[1]);
		exit(np);
	}
	else if(np==4){
		datos = burbuja(datos);
		write(pipefd[1], datos, sizeof(int)*N);
		close(pipefd[1]);
		exit(np);
	}
	else if(np==5){
		histograma(datos, cont);
		write(pipefd[1], cont, sizeof(int)*NUM_DAT);
		close(pipefd[1]);
		exit(np);
	}
}
void proceso_padre(int pipefd[], int datos[], int cont[]){
	pid_t pid;
	register int np;
	int edo_np, resultado;

	printf("Proceso padre con pid %d\n", getpid());
	close(pipefd[1]);
	for(np=0; np<NUM_PROC; np++){
		pid=wait(&edo_np);
		edo_np = edo_np>>8;
		if(edo_np == 4){
			read(pipefd[0], datos, sizeof(int)*N);
			printf("Proceso %d hijo con pid: %d terminado. ORDENAMIENTO: \n", edo_np, pid);
			imprimir(datos);
		}
		else if(edo_np == 5){
			read(pipefd[0], cont, sizeof(int)*NUM_DAT);
			printf("Proceso %d hijo con pid: %d terminado. HISTO: \n", edo_np, pid);
			imprimir_histo(cont);
		}
		else{
			read(pipefd[0], &resultado, sizeof(int));
			printf("Proceso %d hijo con pid: %d, resultado: %d terminado\n", edo_np, pid, resultado);
		}
	}
	close(pipefd[0]);
}