#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#include "defs.h"
#include "arreglos.h"
#include "procesamiento.h"
#include "procesos.h"

int main(){
	pid_t pid;
	register int np;
	int pipefd[2], edo_pipe;
	int *datos, cont[NUM_DAT];

	srand(getpid());
	datos = reservar_memoria();
	llenar_arreglo(datos);
	imprimir(datos);

	printf("Probando procesos...\n");
	edo_pipe = pipe(pipefd);
	if(edo_pipe == -1){
		perror("Error al crear la tuberia");
		exit(EXIT_FAILURE);
	}

	for(np=0; np<NUM_PROC; np++){
		pid=fork();
		if(pid==-1){
			perror("Error al crear el proceso");
			exit(EXIT_FAILURE);
		}
		if(!pid){
			proceso_hijo(np, pipefd, datos, cont);
		}
	}
	proceso_padre(pipefd, datos, cont);
	return 0;
}


/*
*	Busca el mayor->P1
*	Busca el menor->P2
*	Promedio(entero)->P3
*	El que mas se repite->P4
*	define N 4096
*	Ordenamiento Burbuja con pipes
*		write(pipefd[1], arreglo, sizeof(int)*N);
*	Frecuencias(histograma)
*/