#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include "defs.h"

extern float seno[MUESTRAS], ham[MUESTRAS], res[MUESTRAS];

void generaSeno(float seno[]){
	float f=1000, fs=45000;
	register int n;
	for(n=0; n<MUESTRAS; n++){
		seno[n] = sinf(2*M_PI*n*f/fs);
	}
}
void generaHam(float ham[]){
	float a0=0.53836, a1=0.46164;
	register int n;
	for(n=0; n<MUESTRAS; n++){
		ham[n] = (a0-(a1*cos((2*M_PI*n)/MUESTRAS)));
	}
}