#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "defs.h"

extern float seno[MUESTRAS], ham[MUESTRAS], res[MUESTRAS];
extern pthread_mutex_t bloqueo;

void* funHilo(void *arg){
	int nh = *(int *)arg;
	register int i = 0;

	printf("Hilo %d ejecutado con contador\n", nh);

	pthread_mutex_lock(&bloqueo);
	for(i=nh; i<MUESTRAS; i+=NUM_HILOS){
		res[i] = ham[i]*seno[i];
	}
	pthread_mutex_unlock(&bloqueo);

	pthread_exit(arg);
}