#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "defs.h"
#include "archivos.h"
#include "procesamiento.h"
#include "hilos.h"

float seno[MUESTRAS], ham[MUESTRAS], res[MUESTRAS];
pthread_mutex_t bloqueo;

int main(){
	register int nh;
	int nhs[NUM_HILOS], *res_nh;
	pthread_t tids[NUM_HILOS];
	
	generaSeno(seno);
	generaHam(ham);

	pthread_mutex_init(&bloqueo, NULL);

	for( nh=0; nh<NUM_HILOS; nh++){
		nhs[nh] = nh;
		pthread_create(&tids[nh], NULL, funHilo, (void *)&nhs[nh]);
	}
	for( nh=0; nh<NUM_HILOS; nh++){
		pthread_join(tids[nh], (void **)&res_nh);
		printf("Hilo %d terminado.\n", *res_nh);
	}

	pthread_mutex_destroy(&bloqueo);
	guardaDatos(seno, ham, res);
	
	return 0;
}