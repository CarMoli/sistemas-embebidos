#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#include<math.h>
#include "defs.h"

extern float seno[MUESTRAS], ham[MUESTRAS], res[MUESTRAS];

void guardaDatos(float datos[], float ham[], float res[]){
	FILE *apArch, *apHam, *apRes;
	register int n;
	apArch=fopen("seno.dat","w");
	apHam=fopen("hamming.dat","w");
	apRes=fopen("resultados.dat","w");
	if(!apArch || !apHam || !apRes){
		perror("Error al abrir el archivo");
		exit(EXIT_FAILURE);
	}
	for(n=0; n<MUESTRAS; n++){
		fprintf(apArch, "%f\n", datos[n]);
		fprintf(apHam, "%f\n", ham[n]);
		fprintf(apRes, "%f\n", res[n]);
	}
	fclose(apArch);
	fclose(apHam);
	fclose(apRes);
}