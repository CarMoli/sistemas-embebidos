#ifndef PROCESOS
#define PROCESOS

void proceso_hijo(int np, int pipefd[]);
void proceso_padre(int pipefd[NUM_PROC][2]);

#endif