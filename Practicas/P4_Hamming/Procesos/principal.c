#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#include "defs.h"
#include "procesos.h"
#include "procesamiento.h"
#include "archivos.h"

float seno[MUESTRAS], ham[MUESTRAS], res[MUESTRAS];

int main(){	
	pid_t pid;
	register int np;
	int pipefd[NUM_PROC][2], edo_pipe;

	generaSeno(seno);
	generaHam(ham);
	
	printf("\nProbando procesos...\n");
	for(np=0; np<NUM_PROC; np++){
		edo_pipe = pipe(&pipefd[np][0]);
		if(edo_pipe == -1){
			perror("Error al crear la tuberia");
			exit(EXIT_FAILURE);
		}
		pid=fork();
		if(pid==-1){
			perror("Error al crear el proceso");
			exit(EXIT_FAILURE);
		}
		if(!pid){
			proceso_hijo(np, &pipefd[np][0]);
		}
	}
	proceso_padre(pipefd);
	guardaDatos(seno, ham, res);

	return 0;
}