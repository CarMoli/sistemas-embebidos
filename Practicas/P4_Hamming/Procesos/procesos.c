#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#include<math.h>
#include "defs.h"

extern float seno[MUESTRAS], ham[MUESTRAS], res[MUESTRAS];

void proceso_hijo(int np, int pipefd[]){
	register int i;
	int iniBloque, finBloque, tamBloque;
	tamBloque = MUESTRAS/NUM_PROC;
	iniBloque = np * tamBloque;
	finBloque = iniBloque + tamBloque;
	
	printf("Proceso hijo: %d, con pid %d\n", np, getpid());
	close(pipefd[0]);

	for(i=iniBloque; i<finBloque; i++)
		res[i] = ham[i]*seno[i];

	write(pipefd[1], res+iniBloque, sizeof(int)*tamBloque);
	close(pipefd[1]);
	exit(np);
}
void proceso_padre(int pipefd[NUM_PROC][2]){
	pid_t pid;
	register int np;
	int edo_np, iniBloque, tamBloque;
	tamBloque = MUESTRAS/NUM_PROC;

	printf("Proceso padre con pid %d\n", getpid());

	for(np=0; np<NUM_PROC; np++){
		close(pipefd[np][1]);
		pid=wait(&edo_np);
		edo_np = edo_np>>8;
		iniBloque = np*tamBloque;

		read(pipefd[edo_np][0], res+iniBloque, sizeof(int)*tamBloque);
		printf("Proceso %d hijo con pid: %d terminado\n", edo_np, pid);
		close(pipefd[edo_np][0]);
	}
}