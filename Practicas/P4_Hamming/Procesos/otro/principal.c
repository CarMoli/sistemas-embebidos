#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#include<math.h>
#define NUM_PROC 4
#define MUESTRAS 512

void proceso_hijo(int np, int pipefd[]);
void proceso_padre(int pipefd[NUM_PROC][2]);

void generaSeno(float seno[]);
void generaHam(float ham[]);
void guardaDatos(float datos[], float ham[], float res[]);

float seno[MUESTRAS], ham[MUESTRAS], res[MUESTRAS];

int main(){	
	pid_t pid;
	register int np;
	int pipefd[NUM_PROC][2], edo_pipe;

	generaSeno(seno);
	generaHam(ham);
	
	printf("\nProbando procesos...\n");
	for(np=0; np<NUM_PROC; np++){
		edo_pipe = pipe(&pipefd[np][0]);
		if(edo_pipe == -1){
			perror("Error al crear la tuberia");
			exit(EXIT_FAILURE);
		}
		pid=fork();
		if(pid==-1){
			perror("Error al crear el proceso");
			exit(EXIT_FAILURE);
		}
		if(!pid){
			proceso_hijo(np, &pipefd[np][0]);
		}
	}
	proceso_padre(pipefd);
	guardaDatos(seno, ham, res);

	return 0;
}

void proceso_hijo(int np, int pipefd[]){
	register int i;
	
	printf("Proceso hijo: %d, con pid %d\n", np, getpid());
	close(pipefd[0]);

	for(i=np; i<MUESTRAS; i+=NUM_PROC){
		res[i] = ham[i]*seno[i];
	}

	write(pipefd[1], res, sizeof(int)*MUESTRAS);
	close(pipefd[1]);
	exit(np);
}
void proceso_padre(int pipefd[NUM_PROC][2]){
	pid_t pid;
	register int np;
	int edo_np;

	printf("Proceso padre con pid %d\n", getpid());

	for(np=0; np<NUM_PROC; np++){
		close(pipefd[np][1]);
		pid=wait(&edo_np);
		edo_np = edo_np>>8;

		read(pipefd[edo_np][0], res, sizeof(int)*MUESTRAS);
		printf("Proceso %d hijo con pid: %d terminado\n", edo_np, pid);
		close(pipefd[edo_np][0]);
	}
}

void generaSeno(float seno[]){
	float f=1000, fs=45000;
	register int n;
	for(n=0; n<MUESTRAS; n++){
		seno[n] = sinf(2*M_PI*n*f/fs);
	}
}
void generaHam(float ham[]){
	float a0=0.53836, a1=0.46164;
	register int n;
	for(n=0; n<MUESTRAS; n++){
		ham[n] = (a0-(a1*cos((2*M_PI*n)/MUESTRAS)));
	}
}
void guardaDatos(float datos[], float ham[], float res[]){
	FILE *apArch, *apHam, *apRes;
	register int n;
	apArch=fopen("seno.dat","w");
	apHam=fopen("hamming.dat","w");
	apRes=fopen("resultados.dat","w");
	if(!apArch || !apHam || !apRes){
		perror("Error al abrir el archivo");
		exit(EXIT_FAILURE);
	}
	for(n=0; n<MUESTRAS; n++){
		fprintf(apArch, "%f\n", datos[n]);
		fprintf(apHam, "%f\n", ham[n]);
		fprintf(apRes, "%f\n", res[n]);
	}
	fclose(apArch);
	fclose(apHam);
	fclose(apRes);
}