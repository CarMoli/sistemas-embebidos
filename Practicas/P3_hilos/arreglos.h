#ifndef ARREGLOS
#define ARREGLOS

int * reservar_memoria(void);
void llenar_arreglo(int *datos);
void imprimir(int *datos);
void imprimir_histo(int *cont);

#endif