#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include "defs.h"
#include "arreglos.h"
#include "procesamiento.h"

int cont[NUM_DAT];

int main(){
	int *res_histo, *res_burbuja, *res_promedio, *res_frecuencia, *res_mayor, *res_menor, *datos;
	pthread_t tid_mayor, tid_menor, tid_promedio, tid_frecuencia, tid_burbuja, tid_histo;

	srand(getpid());
	datos = reservar_memoria();
	llenar_arreglo(datos);
	imprimir(datos);

	pthread_create( &tid_mayor, NULL, buscar_mayor, (void *)datos);
	pthread_create( &tid_menor, NULL, buscar_menor, (void *)datos);
	pthread_create( &tid_promedio, NULL, promedio, (void *)datos);
	pthread_create( &tid_frecuencia, NULL, frecuencia, (void *)datos);
	pthread_create( &tid_burbuja, NULL, burbuja, (void *)datos);
	pthread_create( &tid_histo, NULL, histograma, (void *)datos);

	pthread_join(tid_mayor, (void **) &res_mayor);
	pthread_join(tid_menor, (void **) &res_menor);
	pthread_join(tid_promedio, (void **) &res_promedio);
	pthread_join(tid_frecuencia, (void **) &res_frecuencia);
	pthread_join(tid_burbuja, (void **) &res_burbuja);
	pthread_join(tid_histo, (void **) &res_histo);

	printf("\nEl mayor es %d\n",*res_mayor);
	printf("El menor es %d\n",*res_menor);
	printf("El promedio es %d\n",*res_promedio);
	printf("La moda es %d\n",*res_frecuencia);
	printf("\nORDENAMIENTO: ");
	imprimir(datos);
	printf("\nHISTOGRAMA: ");
	imprimir_histo(res_histo);

	free(datos);
	return 0;
}