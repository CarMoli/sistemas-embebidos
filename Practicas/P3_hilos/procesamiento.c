#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include "defs.h"

extern int cont[NUM_DAT];

void * buscar_mayor(void *arg){
	register int i;
	static int mayor;
	int *datos=(int *)arg;

	mayor = datos[0];
	for ( i = 0; i < N; i++){
		if (datos[i] > mayor){
			mayor = datos[i];
		}
	}
	pthread_exit( (void *)&mayor );
}
void * buscar_menor(void *arg){
	register int i;
	static int menor;
	int *datos=(int *)arg;

	menor = datos[0];
	for (i = 0; i < N; i++){
		if (datos[i] < menor){
			menor = datos[i];
		}
	}
	pthread_exit( (void *)&menor );
}
void * promedio(void *arg){
	register int i;
	static int prom = 0;
	int *datos = (int *)arg;
	for(i=0; i<N; i++){
		prom += datos[i];
	}
	prom = prom/N;
	pthread_exit( (void *)&prom );
}
void * frecuencia(void *arg){
	register int i,j;
	static int moda=0;
	int cont[NUM_DAT]={0}, temp=0;
	int *datos=(int *)arg;
	for(i=0; i<NUM_DAT; i++){
		for(j=0; j<N; j++){
			if(i==datos[j]){
				cont[i]++;
			}
		}
	}
	for(i=0; i<NUM_DAT; i++){
		if(temp<=cont[i]){
			temp = cont[i];
			moda = i;
		}
	}
	pthread_exit( (void *)&moda );
}
void * burbuja(void *arg){
	register int pasada, cuenta;
	int temp;
	int *datos=(int *)arg;

	for(pasada=1; pasada<N; pasada++){
		for(cuenta=0; cuenta<N-1; cuenta++){
			if(datos[cuenta]>datos[cuenta+1]){
				temp = datos[cuenta];
				datos[cuenta] = datos[cuenta+1];
				datos[cuenta+1] = temp;
			}
		}
	}
	pthread_exit( (void *)datos );
}
void * histograma(void *arg){
	register int i,j;
	int *datos=(int *)arg;

	for(i=0; i<NUM_DAT; i++){
		cont[i] = 0;
	}

	for(i=0; i<NUM_DAT; i++){
		for(j=0; j<N; j++){
			if(i==datos[j]){
				cont[i]++;
			}
		}
	}
	pthread_exit( (void *)cont );
}