#ifndef PROCESAMIENTO
#define PROCESAMIENTO

void * buscar_mayor(void *arg);
void * buscar_menor(void *arg);
void * promedio(void *arg);
void * frecuencia(void *arg);
void * burbuja(void *arg);
void * histograma(void *arg);

#endif