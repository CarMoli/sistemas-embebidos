#ifndef PROCESAMIENTO_H
#define PROCESAMIENTO_H

int buscar_mayor(int *datos);
int buscar_menor(int *datos);
float promedio(int *datos);
int frecuencia(int *datos);

#endif