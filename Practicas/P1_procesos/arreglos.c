#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#include "defs.h"

int *reservar_memoria(void){
	int *mem;
	mem = (int *)malloc(sizeof(int)*N);
	if(mem == NULL){
		perror("Error de asignacion de memoria");
		exit(EXIT_FAILURE);
	}
	return mem;
}
void llenar_arreglo(int *datos){
	register int i;

	for(i=0; i<N; i++){
		datos[i] = rand()%NUM_DAT;
	}
}
void imprimir(int *datos){
	register int i;
	for(i=0; i<N; i++){
		if(!(i%16))
			printf("\n");
		printf("%3d ", datos[i]);
	}
	printf("\n");
}