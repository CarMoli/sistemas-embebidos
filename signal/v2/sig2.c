#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#define PID 5218

int main(){
	kill( PID, SIGINT );
	sleep(5);
	kill( PID, SIGTERM );
	sleep(5);
	kill( PID, SIGKILL );
	sleep(5);

	return 0;
}

/*
Practica
-Obtener ubicacion a partir del módulo GPS conectado por UART
-Usar la cadena GPGLL
-Pal viernes 18.10.19
*/