#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#include <signal.h>
#define NUM_PROC 8

void proceso_hijo( int np );
void proceso_padre(int pid[]);
void isr(int sig);

int main()
{
	pid_t pid[NUM_PROC];
	register int np;

	printf("Probando procesos...\n");
	if( signal(SIGUSR1, isr) == SIG_ERR){
		perror("Error en la isr");
		exit(EXIT_FAILURE);
	}

	for( np = 0; np < NUM_PROC; np++ )
	{
		pid[np] = fork();
		if( pid == -1 )
		{
			perror("Error al crear el proceso");
			exit(EXIT_FAILURE);
		}
		if( !pid )
		{
			proceso_hijo( np );
		}
	}
	proceso_padre( pid );

	return 0;
}

void proceso_hijo( int np ){
	printf("Proceso hijo %d con pid %d\n", np, getpid());
	pause();
	printf("Señal detectada en el hijo %d \n", np);
	exit(np);
}

void proceso_padre(int pid[]){
	register int np;
	int status;
	pid_t pid;

	printf("proceso padre con pid %d\n", getpid());

	for( np = 0; np < NUM_PROC; np++ )
	{
		kill( pid[np], SIGUSR1 );
	}

	for( np = 0; np < NUM_PROC; np++ )
	{
		pid = wait( &status );
		printf("proceso hijo %d terminado con PID %d\n", status>>8, pid);
	}
}
void isr(int sig){
	if(sig == SIGUSR1){
		printf("Señal USR1 detectada \n");
	}
}