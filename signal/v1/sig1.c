#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>

void isr(int sig);

int main(){
	if( signal( SIGINT, isr )  == SIG_ERR ){
		perror("Error en la ISR");
		exit(EXIT_FAILURE);
	}
	if( signal( SIGTERM, isr )  == SIG_ERR ){
		perror("Error en la ISR");
		exit(EXIT_FAILURE);
	}

	while(1);
	pause();
	return 0;
}

void isr(int sig){
	if(sig == SIGINT){
		printf("No voy a terminar!! XDxddxDXd \n");
	}
	if(sig == SIGTERM){
		printf("No molestes!! XDxddxDXd \n");
	}
}

/*
Practica
-Obtener ubicacion a partir del módulo GPS conectado por UART
-Usar la cadena GPGLL
-Pal viernes 18.10.19
*/