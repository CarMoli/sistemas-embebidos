#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#define NUM_PROC 4
#define N 64

void proceso_hijo(int np, int pipefd[]);
void proceso_padre(int pipefd[2]);

void imprimir(int *datos);
void llenar_arreglo(int *datos);
int *reservar_memoria(void);

int *A, promedio;

int main(){	
	pid_t pid;
	register int np;
	int pipefd[2], edo_pipe;

	A = reservar_memoria();
	llenar_arreglo(A);
	imprimir(A);

	printf("\nProbando procesos...\n");

	edo_pipe = pipe(pipefd);
	if(edo_pipe == -1){
		perror("Error al crear la tuberia");
		exit(EXIT_FAILURE);
	}
	for(np=0; np<NUM_PROC; np++){
		pid=fork();
		if(pid==-1){
			perror("Error al crear el proceso");
			exit(EXIT_FAILURE);
		}
		if(!pid){
			proceso_hijo(np, pipefd);
		}
	}
	proceso_padre(pipefd);
	printf("El promedio es: %d\n", promedio);
	free(A);

	return 0;
}
void proceso_hijo(int np, int pipefd[]){
	register int i;
	/*int iniBloque, finBloque, tamBloque;
	tamBloque = N/NUM_PROC;
	iniBloque = np * tamBloque;
	finBloque = iniBloque + tamBloque;*/
	
	promedio=0;
	printf("Proceso hijo: %d, con pid %d\n", np, getpid());
	close(pipefd[0]);

	for(i=np; i<N; i+=NUM_PROC){
		promedio += A[i];
	}

/*	for(i=iniBloque; i<finBloque; i++)
		promedio += A[i];
*/
	write(pipefd[1], &promedio, sizeof(int));
	close(pipefd[1]);
	exit(np);
}
void proceso_padre(int pipefd[2]){
	pid_t pid;
	register int np;
	int edo_np, suma_parcial;

	printf("Proceso padre con pid %d\n", getpid());
	close(pipefd[1]);
	promedio=0;

	for(np=0; np<NUM_PROC; np++){
		pid=wait(&edo_np);
		edo_np = edo_np>>8;

		read(pipefd[0], &suma_parcial, sizeof(int));
		promedio += suma_parcial;

		printf("Proceso %d hijo con pid: %d terminado\n", edo_np, pid);
	}
	promedio >>= 6;
	close(pipefd[0]);
}

void imprimir(int *datos){
	register int i;
	for(i=0; i<N; i++){
		if(!(i%16))
			printf("\n");
		printf("%3d ", datos[i]);
	}
	printf("\n");
}
void llenar_arreglo(int *datos){
	register int i;

	for(i=0; i<N; i++){
		datos[i] = rand()%256;
	}
}
int *reservar_memoria(void){
	int *mem;
	mem = (int *)malloc(sizeof(int)*N);
	if(mem == NULL){
		perror("Error de asignacion de memoria");
		exit(EXIT_FAILURE);
	}
	return mem;
}

/*

PRACTICA VIERNES
-Hacer una funcion que calcule los elementos
de la ventana Hamming. N=512
-Guardar los elementos en un archivo
-Generar sig elementos de la señal senoidal discreta
y guardarlos en un archivo
-Multiplicar Hamming * Senoidal = P
guardar P en un archivo
-Procesos e hilos

*/