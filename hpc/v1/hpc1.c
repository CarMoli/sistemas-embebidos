#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#define N 64

int *A, *B, *P;
int *reservar_memoria(void);
void llenar_arreglo(int *datos);
void imprimir(int *datos);
void producto(void);

int main(){
	A = reservar_memoria();
	llenar_arreglo(A);
	imprimir(A);

	B = reservar_memoria();
	llenar_arreglo(B);
	imprimir(B);

	P = reservar_memoria();

	producto();

	free(A);
	free(B);
	free(P);

	return 0;
}
void producto(void){
	register int i;
	for(i=0; i<N; i++){
		P[i] = A[i] * B[i];
		printf("%d ", P[i]);
	}
	printf("\n");
}

void imprimir(int *datos){
	register int i;
	for(i=0; i<N; i++){
		if(!(i%16))
			printf("\n");
		printf("%3d ", datos[i]);
	}
	printf("\n");
}
void llenar_arreglo(int *datos){
	register int i;

	for(i=0; i<N; i++){
		datos[i] = rand()%256;
	}
}
int *reservar_memoria(void){
	int *mem;
	mem = (int *)malloc(sizeof(int)*N);
	if(mem == NULL){
		perror("Error de asignacion de memoria");
		exit(EXIT_FAILURE);
	}
	return mem;
}
