#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#define NUM_HILOS 4
#define N 32

int *A, promedio;
pthread_mutex_t bloqueo;

void* funHilo(void *arg);
void imprimir(int *datos);
void llenar_arreglo(int *datos);
int *reservar_memoria(void);

int main(){
	register int nh;
	int nhs[NUM_HILOS], *res_nh;
	pthread_t tids[NUM_HILOS];
	
	A = reservar_memoria();
	llenar_arreglo(A);
	imprimir(A);

	promedio=0;
	pthread_mutex_init(&bloqueo, NULL);

	for( nh=0; nh<NUM_HILOS; nh++){
		nhs[nh] = nh;
		pthread_create(&tids[nh], NULL, funHilo, (void *)&nhs[nh]);
	}
	for( nh=0; nh<NUM_HILOS; nh++){
		pthread_join(tids[nh], (void **)&res_nh);
		printf("Hilo %d terminado.\n", *res_nh);
	}
	promedio = promedio>>5;
	printf("El promedio es: %d\n", promedio);

	pthread_mutex_destroy(&bloqueo);
	free(A);

	return 0;
}

void* funHilo(void *arg){
	int nh = *(int *)arg;
	register int i = 0;
	int suma_parcial=0;

	printf("Hilo %d ejecutado con contador\n", nh);

	for(i=nh; i<N; i+=NUM_HILOS){
		suma_parcial += A[i];
	}
	pthread_mutex_lock(&bloqueo);
	promedio += suma_parcial;
	pthread_mutex_unlock(&bloqueo);

	pthread_exit(arg);
}
/*
void* funHilo(void *arg){
	int nh = *(int *)arg;
	int tamBloque = N/NUM_HILOS;
	int iniBloque = nh * tamBloque;
	int finBloque = iniBloque + tamBloque;
	register int i = 0;

	printf("Hilo %d ejecutado con contador\n", nh);

	for(i=iniBloque; i<finBloque; i++){
		P[i] = A[i]*B[i];
	}

	pthread_exit(arg);
}*/
void imprimir(int *datos){
	register int i;
	for(i=0; i<N; i++){
		if(!(i%16))
			printf("\n");
		printf("%3d ", datos[i]);
	}
	printf("\n");
}
void llenar_arreglo(int *datos){
	register int i;

	for(i=0; i<N; i++){
		datos[i] = rand()%256;
	}
}
int *reservar_memoria(void){
	int *mem;
	mem = (int *)malloc(sizeof(int)*N);
	if(mem == NULL){
		perror("Error de asignacion de memoria");
		exit(EXIT_FAILURE);
	}
	return mem;
}

//reentrancia
//condicion de carrera